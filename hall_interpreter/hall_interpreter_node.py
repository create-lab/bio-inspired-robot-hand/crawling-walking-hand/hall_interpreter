import rclpy
from rclpy.node import Node
from std_msgs.msg import Int32MultiArray, Float64MultiArray
import numpy as np

class HallInterpreterNode(Node):

    def __init__(self):
        super().__init__('hall_interpreter_node')

        # Tendon position publisher
        self.hall_publisher = self.create_publisher(Float64MultiArray, '/hall_data', 10)
        self.glove_joint_subscriber = self.create_subscription(Int32MultiArray, '/raw_hall_data', self.hall_callback, 10)

        # Init hall data array
        self.hall_data = None
        self.zero_values = [0] * 15

    def hall_callback(self, msg:Int32MultiArray):

        # Initial data
        if self.hall_data is None:
            for i in range(15):
                self.zero_values[i] = msg.data[i]
            self.hall_data = [0.0] * 5

        # Process hall data
        else:
            for i in range(5):
                x = msg.data[3*i] - self.zero_values[3*i]
                y = msg.data[3*i + 1] - self.zero_values[3*i + 1]
                z = msg.data[3*i + 2] - self.zero_values[3*i + 2]

                out = np.sqrt(x**2 + y**2 + z**2) / 100

                self.hall_data[i] = out
            
            # publish tendon position data
            hall_output = Float64MultiArray()
            hall_output.data = self.hall_data

            self.hall_publisher.publish(hall_output)

def main(args=None):
    rclpy.init(args=args)

    hall_interpreter_node = HallInterpreterNode()

    rclpy.spin(hall_interpreter_node)

    hall_interpreter_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()